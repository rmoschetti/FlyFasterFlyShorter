FlyFasterFlyShorter

“Fly faster? Fly shorter!” is an educational math game (designed for one-player or two-players mode). 
Players fly a plane that cannot change speed, so they can win only by following the shortest route between the checkpoints they have to reach. 
But which is the shortest route on a map? Test your skill!

The code is released under the Licence Apache 2.0