if (UseGamepad) {
		window.IU="<img src=\"files/images/b1_g.png\" width=\"80\" style=\"vertical-align:middle\">";
		window.IL="<img src=\"files/images/a1_g.png\" width=\"80\" style=\"vertical-align:middle\">";
		window.Klang="<img src=\"files/images/b0_g.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kgr="<img src=\"files/images/b2_g.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kve="<img src=\"files/images/b3_g.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kre="<img src=\"files/images/b10_g.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kco="<img src=\"files/images/b9_g.png\" width=\"50\" style=\"vertical-align:middle\">";
	}
	else { 
		window.IU="<img src=\"files/images/up.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.IL="<img src=\"files/images/left.png\" width=\"50\" style=\"vertical-align:middle\"> / <img src=\"files/images/right.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Klang="<img src=\"files/images/l.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kgr="<img src=\"files/images/g.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kve="<img src=\"files/images/v.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kre="<img src=\"files/images/o.png\" width=\"50\" style=\"vertical-align:middle\">";
		window.Kco="<img src=\"files/images/p.png\" width=\"50\" style=\"vertical-align:middle\">";
	}

window.NameImagesP=["files/images/equirettangolareP.jpg","files/images/mercatoreP.jpg","files/images/azimutaleP.png","files/images/gallpetersP.jpg","files/images/fullercubeP.png","files/images/sinusoidaleP.png"];
window.NameImages=["files/images/equirettangolare.jpg","files/images/mercatore.jpg","files/images/azimutale.jpg","files/images/gallpeters.jpg","files/images/fullercube.jpg","files/images/sinusoidale.jpg"];
window.NameImagesG=["files/images/equirettangolareG.jpg","files/images/mercatoreG.jpg","files/images/azimutaleG.jpg","files/images/gallpetersG.jpg","files/images/fullercubeG.jpg","files/images/sinusoidaleG.jpg"];
window.NameLayers=["DivCanvas","DivCanvasMe","DivCanvasAzi","DivCanvasGP","DivCanvasFul","DivCanvasSin"];

window.MapNames=[
["","EQUIRECTANGULAR",
	"EQUIRETTANGOLARE",
	"PLATE CARRÉE",
	"EBENA"],
["","MERCATOR",
	"MERCATORE",
	"MERCATOR",
	"MERCATOR"],
["","AZIMUTHAL EQUIDISTANT",
	"AZIMUTALE EQUIDISTANTE",
	"AZIMUTALE ÉQUIDISTANTE",
	"ACIMUTALA SAMDISTANCA"],
["","GALL-PETERS",
	"GALL-PETERS",
	"GALL-PETERS",
	"GALL-PETERS"],
["","CUBIC FULLER",
	"FULLER CUBICA",
	"FULLER CUBIQUE",
	"FULLER KUBA"],
["","SINUSOIDAL",
	"SINUSOIDALE",
	"SINUSOÏDALE",
	"SINUSA"]
];

window.TranslateText=[
["Tpress",	"PRESS "+IU+" TO PLAY",
			"PREMI "+IU+" PER GIOCARE",
			"APPUYEZ SUR "+IU+" POUR JOUER",
			"PREMU "+IU+" POR LUDI"],
["Ttochange",	"USE " + IL + " TO SELECT 1 OR 2 PLAYERS",
				"USA " + IL + " PER PASSARE DA 1 A 2 GIOCATORI",
				"APPUYEZ SUR " + IL + " POUR 1 OU 2 JOUEURS",
				"PREMU " + IL + " POR ELEKTI 1 AŬ 2 LUDANTOJN"],
["Tsceglicartina",	"CHOOSE A MAP WITH " + IL + "</br>THEN PRESS "+IU+" TO CONFIRM",
					"SELEZIONA UNA MAPPA CON " + IL + "</br>E PREMI "+IU+" PER CONFERMARE",
					"CHOISISSEZ UNE CARTE EN APPUYANT SUR " + IL + "</br>ENSUITE APPUYEZ SUR "+IU+" POUR CONFIRMER",
					"ELEKTU MAPON PER " + IL + "</br>TIAM PREMU "+IU+" PRO KONFIRMI"],
["Tgetall",	"GET ALL THE " + LengthPath + " CHECKPOINTS THAT WILL APPEAR",
			"RAGGIUNGI TUTTI I " + LengthPath + " CHECKPOINT CHE COMPARIRANNO",
			"ATTEIGNEZ TOUS LES " + LengthPath + " POINTS DE CONTRÔLE QUI APPARAISSENT",
			"ATINGU ĈIUJ LA " + LengthPath + " APERONTAJN KONTROLPUNKTOJN"],
["Ttocontinue",	"PRESS "+IU+" TO CONTINUE",
				"PREMI "+IU+" PER CONTINUARE",
				"APPUYEZ SUR "+IU+" POUR CONTINUER",
				"PREMU "+IU+" POR DAŬRIGI"],
["Tyou","YOU CAN NOT ACCELERATE NOR DECELERATE<br/>YOU CAN ONLY TURN",
		"NON PUOI ACCELERARE O DECELERARE<br/>PUOI SOLO CURVARE",
		"VOUS NE POUVEZ PAS ACCÉLÉRER OU DÉCÉLÉRER<br/>VOUS POUVEZ SEULEMENT TOURNER",
		"VI NE POVAS AKĈELI NEK MALAKĈELI<br/>VI POVAS NUR TURNI"],
["Ttostart",	"PRESS "+IU+" TO START",
				"PREMI "+IU+" PER INIZIARE",
				"APPUYEZ SUR "+IU+" POUR COMMENCER",
				"PREMU "+IU+" POR KOMENCI"],
["Tend","GAME OVER! </br>PRESS "+IU+" TO PLAY AGAIN",
		"LA PARTITA È FINITA! </br>PREMI "+IU+" PER GIOCARE ANCORA",
		"JEU TERMINÉ! </br>APPUYEZ SUR "+IU+" POUR JOUER À NOUVEAU",
		"LA LUDO FINIĜIS! </br>PREMU "+IU+" POR LUDI DENOVE"],
["Tavv","PLUG IN THE TWO GAMEPADS AND PRESS "+IU+" UNTIL THE TEXT \"ENABLED\" APPEARS. TO START TURN RIGHT WITH THE TWO GAMEPADS AT THE SAME TIME.",
		"COLLEGA I DUE GAMEPAD E PREMI "+IU+" FINO A QUANDO NON COMPARE LA SCRITTA \"ABILITATO\". PER AVVIARE IL GIOCO STERZA CONTEMPORANEAMENTE A DESTRA CON ENTRAMBI I GAMEPAD.",
		"BRANCHEZ LES DEUX MANETTES ET APPUYEZ SUR "+IU+" JUSQU'À LE TEXTE \"ACTIVÉE\" APPARAÎT. POUR COMMENCER TOURNEZ À DROITE AVEC LES DEUX MANETTES À LA FOIS.",
		"KONEKTIGU LA DU LUDOTABULETOJ KAJ PREMU "+IU+" ĜIS LA TEKSTO \"AKTIVIGITA\" APERAS. POR KOMENCI TURNU DEKSTRE KUN LA DU LUDOTABULETOJ SAMTEMPE."],
["Tcal","DON'T TOUCH THE GAMEPADS WHILE THE CALIBRATION IS IN PROGRESS. THE GAME WILL AUTOMATICALLY GO TO THE NEXT PHASE.",
		"LASCIA I GAMEPAD IN POSIZIONE CENTRALE MENTRE È IN CORSO LA CALIBRAZIONE. IL PROGRAMMA PASSERÀ IN AUTOMATICO ALLA FASE SUCCESSIVA.",
		"NE TOUCHEZ PAS LES MANETTES LORSQUE LE CALIBRAGE EST EN COURS. LE JEU PASSERA AUTOMATIQUEMENT À LA PHASE SUIVANTE.",
		"NE TUŜU LA LUDOTABULETOJN DUM LA KALIBRADO ESTAS PROGRESANTA. LA LUDO AŬTOMATE IROS AL LA SEKVA FAZO."],
["Tbreak",	"WHEN YOU REACH A CHECKPOINT<br/> YOUR PLANE STOPS FOR 3 SECONDS",
			"QUANDO RAGGIUNGI UN CHECKPOINT</br> IL TUO AEREO SI FERMA PER 3 SECONDI", 
			"LORSQUE VOUS ATTEIGNEZ UN POINT DE CONTRÔLE<br/> VOTRE AVION S'ARRÊTE PENDANT 3 SECONDES",
			"KIAM VI ATINGAS KONTROLPUNKTON<br/> VIA AVIADILO HALTAS DUM 3 SEKUNDOJ"],
["Tsecondi","SECONDS", 
			"SECONDI", 
			"SECONDES", 
			"SEKUNDOJ"],
["Tred",	"RED",
			"ROSSO", 
			"ROUGE",
			"RUĜA"],
["Tgreen",	"GREEN",
			"VERDE", 
			"VERT",
			"VERDA"],
["Tcirc",	"CIRCLE",
			"CERCHIO", 
			"CERCLE",
			"RONDO"],
["Tsquare",	"SQUARE",
			"QUADRATO", 
			"CARRÉ",
			"KVADRATO"],
["Tared",	"RED PLANE",
			"AEREO ROSSO", 
			"AVION ROUGE",
			"RUĜA AVIADILO"],
["Tagreen",	"GREEN PLANE",
			"AEREO VERDE", 
			"AVION VERT",
			"VERDA AVIADILO"],
["Tacircle","CIRCLE",
			"CERCHIO", 
			"CERCLE",
			"RONDO"],
["Tasquare","SQUARE",
			"QUADRATO", 
			"CARRÉ",
			"KVADRATO"],
["Tenable",	"ENABLED",
			"ABILITATO", 
			"ACTIVÉE",
			"AKTIVIGITA"],
["Ttolanguage",	"PRESS " + Klang + " TO CHANGE LANGUAGE",
				"PREMI " + Klang + " PER CAMBIARE LINGUA",
				"APPUYEZ SUR " + Klang + " POUR CHANGER LA LANGUE",
				"PREMU " + Klang + " POR ŜANĜI LINGVON"],
["Tgr",			"PRESS " + Kgr + " TO SHOW/HIDE PARALLELS AND MERIDIANS",
				"PREMI " + Kgr + " PER MOSTRARE/NASCONDERE MERIDIANI E PARALLELI", 
				"APPUYEZ SUR " + Kgr + " POUR AFFICHER/CACHER PALALLÈLES ET MÉRIDIENS",
				"PREMU " + Kgr + " POR MONTRI/KAŜI PARALELAJ KAJ MERIDIANOJ"],
["Tve",			"PRESS " + Kve + " TO CHANGE PLANE VISUALIZATION",
				"PREMI " + Kve + " PER CAMBIARE LA VISUALIZZAZIONE DELL’AEREO", 
				"APPUYEZ SUR " + Kve + " POUR MODIFIER LA VISUALISATION DE L'AVION",
				"PREMU " + Kve + " POR ŜANĜI LA VIDIGO DE LA AVIADILO"],
["Tre",			"PRESS " + Kre + " TO RETURN TO HOME SCREEN",
				"PREMI " + Kre + " PER TORNARE ALLA SCHERMATA INIZIALE",
				"APPUYEZ SUR " + Kre + " POUR REVENIR À L'ÉCRAN D'ACCUEIL",
				"PREMU " + Kre + " POR REVENI AL LA HEJMA EKRANO"],
["Tco",			"PRESS " + Kco + " TO TOGGLE COLORBLIND MODE",
				"PREMI " + Kco + " PER ATTIVARE O DISATTIVARE LA MODALITÀ DALTONICI",
				"APPUYEZ SUR " + Kco + " POUR ACTIVER OU DEACTIVER LE MODE DALTONIEN",
				"PREMU " + Kco + " POR AKTIVIGI AŬ MALAKTIVIGI KOLORBLINDOJ MODON"],
["Taltri",	"OTHER CONTROLS",
			"ALTRI COMANDI", 
			"AUTRES CONTRÔLES",
			"ALIAJ KONTROLOJ"],
["Tload",	"LOADING, PLEASE WAIT",
			"CARICAMENTO, ATTENDERE", 
			"CHARGEMENT EN COURS, ATTENDRE S.V.P.",
			"ŜARĜANTE, BONVOLU ATENDU"],
["NColorBlind",	"COLORBLIND MODE", 
				"MOD. DALTONICI", 
				"MODE DALTONIEN",
				"KOLORBLINDOJ MODO"]
];

window.Flags=["","files/images/flags/united_kingdom_round_icon_64.png","files/images/flags/italy_round_icon_64.png","files/images/flags/france_round_icon_64.png","files/images/flags/esperanto_round_icon_64.png"]

function Traduci(L) {
	for (var t=0;t<TranslateText.length;t++) {
		var vals = [].slice.call(document.getElementsByName(TranslateText[t][0]));
		for( var i=0; i<vals.length; ++i ) {
			vals[i].innerHTML=TranslateText[t][L];	
		}
	}
	document.getElementById("NomeCartinaAttiva").innerHTML=MapNames[CartinaAttiva][L];
	document.getElementById("ImgLingua").src=Flags[L];
}
