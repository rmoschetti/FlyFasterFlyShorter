function HideDiv() {
	var DivToHide=["DebugScreen","DebugScreen2","SplashScreen0","SplashScreen1","SplashScreen2","SplashScreen3","Guida1","Guida2","Guida3","Guida4","Timer","TestPulsante0","TestPulsante1"];
	for (var j=0;j<DivToHide.length;j++)
		document.getElementById(DivToHide[j]).style.visibility='hidden';
}

function NuovaFase(N) {
	if ((StepProgramma==0 ) & N==0.5) {
		var SecMancanti=3;
			document.getElementById("TempoPasPerCalibrazione").innerHTML=SecMancanti;
			ConteggioCalibrazione=setInterval(function () {
					document.getElementById("TempoPasPerCalibrazione").innerHTML=SecMancanti;
					if (--SecMancanti < 0) {
						clearInterval(ConteggioCalibrazione);
					}
    			}, 1000);
		setTimeout(Calibra,2000);
		HideDiv();
		document.getElementById("SplashScreen0").style.visibility='visible';
		document.getElementById("DebugScreen2").style.visibility='visible';
		StepProgramma=0.5;
		AbilitaTasti();
	}
	
	
	if ( N==1) {
		AereiInGioco=1;
		document.getElementById("one").style.borderColor="rgba(255,0,0,1)";
		document.getElementById("two").style.borderColor="rgba(255,0,0,0)";
		for (i=0;i<Punti.length;i++) {
			Punti[i].Reset();	
		}
		
		//document.getElementById("TempoPartita").innerHTML="";
		//document.getElementById("Timer").innerHTML="";
		HideDiv();
		document.getElementById("SplashScreen0").style.visibility='visible';
		document.getElementById("SplashScreen1").style.visibility='visible';
		StepProgramma=1;
	}
	
	if (StepProgramma==1 & N==2) {
		if (AereiInGioco==2) {
			for (i=0;i<Punti.length;i++) {
				Punti[i].Attivo=1;
				Punti[i].GeneraImmagine();	
			}
		}
		StepProgramma=2;
		NuovaFase(3);
	}	
	
	
	if (StepProgramma==2 & N==3) {
		StepProgramma=3;
		//document.getElementById("TempoPartita").innerHTML="";
		document.getElementById("RisultatiAerei").innerHTML="";
		HideDiv();
		document.getElementById("SplashScreen0").style.visibility='visible';
		document.getElementById("SplashScreen2").style.visibility='visible';	
	}
	
	if (StepProgramma==3 & N==3.5) {
		StepProgramma=3.5;
		HideDiv();
		document.getElementById("SplashScreen0").style.visibility='visible';
		document.getElementById("Guida1").style.visibility='visible';	
	}
	
	if (StepProgramma==3.5 & N==3.6) {
		StepProgramma=3.6;
		HideDiv();
		document.getElementById("SplashScreen0").style.visibility='visible';
		document.getElementById("Guida2").style.visibility='visible';	
	}
	
	if (StepProgramma==3.6 & N==3.7) {
		StepProgramma=3.7;
		HideDiv();
		document.getElementById("SplashScreen0").style.visibility='visible';
		document.getElementById("Guida3").style.visibility='visible';	
	}
	
		if (StepProgramma==3.7 & N==3.8) {
		StepProgramma=3.8;
		HideDiv();
		document.getElementById("SplashScreen0").style.visibility='visible';
		document.getElementById("Guida4").style.visibility='visible';	
	}
	
	if (N==4) {
		StepProgramma=4;
		HideDiv();
		document.getElementById("Timer").style.visibility='visible';	
		IniziaPartita();
		
		
	}
	
	if (StepProgramma==4 & N==5) {
		Tempo0=Punti[0].TempoImpiegato;
		Tempo1=Punti[1].TempoImpiegato;
		StepProgramma=5;
		HideDiv();
		document.getElementById("SplashScreen3").style.visibility='visible';
		document.getElementById("Timer").style.visibility='visible';
		//document.getElementById("TempoPartita").innerHTML="";
			if (Tempo0>0) {
				if (GoingCB==1) document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#D00;'><span name=\"Tacircle\">secondi</span>:</b>&nbsp;" + Math.floor((Tempo0-3000)/100)/10 + "&nbsp; <span name=\"Tsecondi\">secondi</span> <br><br>";	
				else document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#D00;'><span name=\"Tared\">secondi</span>:</b>&nbsp;" + Math.floor((Tempo0-3000)/100)/10 + "&nbsp; <span name=\"Tsecondi\">secondi</span> <br><br>";	
			}
			if (Tempo0==-1) {
				if (GoingCB==1) document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#D00;'><span name=\"Tacircle\">secondi</span>:</b>&nbsp; --- &nbsp; <span name=\"Tsecondi\">secondi</span> <br><br>";	
				else document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#D00;'><span name=\"Tared\">secondi</span>:</b>&nbsp; --- &nbsp; <span name=\"Tsecondi\">secondi</span> <br><br>";	
			}
			
			if (Tempo1>0) {
				if (GoingCB==1) document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#0D0;'><span name=\"Tasquare\">secondi</span>:</b>&nbsp;" + Math.floor((Tempo1-3000)/100)/10 + "&nbsp; <span name=\"Tsecondi\">secondi</span> ";	
				else document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#0D0;'><span name=\"Tagreen\">secondi</span>:</b>&nbsp;" + Math.floor((Tempo1-3000)/100)/10 + "&nbsp; <span name=\"Tsecondi\">secondi</span> ";	
			}
			if (Tempo1==-1) {
				if (GoingCB==1) document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#0D0;'><span name=\"Tasquare\">secondi</span>:</b>&nbsp; --- &nbsp; <span name=\"Tsecondi\">secondi</span> <br><br>";	
				else document.getElementById("RisultatiAerei").innerHTML+="<b style='color:#0D0;'><span name=\"Tagreen\">secondi</span>:</b>&nbsp; --- &nbsp; <span name=\"Tsecondi\">secondi</span> <br><br>";	
			}
		Traduci(CurrentLang);	
		
		//RisultatiAerei
		AStop();
		NewTarget();
	}
	
}