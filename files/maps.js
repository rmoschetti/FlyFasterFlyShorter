// Funzioni che portano le coordinate dal canvas a quelle dell'equirettangolare e viceversa
function CaToEq(P) {
	return [-Math.PI+(P[0]/w)*2*Math.PI,(Math.PI)/2-(P[1]/h)*Math.PI];
}
function EqToCa(P) {
	return [(P[0]+Math.PI)*w/(2*Math.PI),((Math.PI)/2-P[1])*(h)/(Math.PI)];
}
function VecCaToEq(V) {
	return [V[0]*(2*Math.PI)/w,-V[1]*(Math.PI)/h];
}
function VecEqToCa(V) {
	return [V[0]*w/(2*Math.PI),-V[1]*h/(Math.PI)];
}


function MeToCa(P) {
	return [(P[0]+Math.PI)*wme/(2*Math.PI),hme-hme*(P[1]-MercTruncDw)/(MercTruncUp-MercTruncDw)];
}
// Da controllare
function CaToMe(P) {
	return [-Math.PI+(P[0]/wme)*2*Math.PI,-(P[1]-hme)*(MercTruncUp-MercTruncDw)/hme+MercTruncDw];
}
function VecMeToCa(V) {
	return [V[0]*wme/(2*Math.PI),-hme*V[1]/(MercTruncUp-MercTruncDw)];
}
function VecCaToMe(V) {
	return [(V[0]/w)*2*Math.PI,-V[1]*(MercTruncUp-MercTruncDw)/hme];
}


// Funzioni che portano le coordinate dell'equirettangolare a quelle della mercatore (COMPLETA) e viceversa
function EqToMe(P) {
	return [P[0],Math.log(Math.tan((Math.PI/4)+(P[1]/2)))];
}
function MeToEq(P) {
	return [P[0],2*Math.atan(Math.exp(P[1]))-Math.PI/2];
}
function VecEqToMe(P,V) {
	return [V[0],V[1]/(Math.cos(P[1]))];
}
function VecMeToEq(P,V) {
	return [V[0],V[1]*Math.cos(P[1])];
}

// Funzioni che portano le coordinate dell'equirettangolare a quelle della azimutale equidistante e viceversa
function EqToAz(P) {
	return [(Math.PI/2-P[1])*Math.sin(P[0]),-(Math.PI/2-P[1])*Math.cos(P[0])];
}
function AzToCa(P) {
	return [waz/2+waz*P[0]/(2*Math.PI),haz/2-haz*P[1]/(2*Math.PI)];
}
function VecEqToAz(P,V) {
	return [V[0]*(Math.PI/2-P[1])*Math.cos(P[0])-V[1]*Math.sin(P[0]),
			V[0]*(Math.PI/2-P[1])*Math.sin(P[0])+V[1]*Math.cos(P[0])];
}
function VecAzToCa(V) {
	return [waz*V[0]/(2*Math.PI),-haz*V[1]/(2*Math.PI)];
}

// Funzioni che portano le coordinate dell'equirettangolare a quelle della gnomonica e viceversa
// Gnomonica di centro [R,0,0]
function EqToGnpX(P) {
	if ((P[0]>-Math.PI/2) & (P[0]<Math.PI/2)) {
		return [R*Math.sin(P[0])/Math.cos(P[0]),R*Math.sin(P[1])/(Math.cos(P[1])*Math.cos(P[0]))];
	}
	else {
		return [1000000000,1000000000];
	}
}
function VecEqToGnpX(P,V) {
	if ((P[0]>-Math.PI/2) & (P[0]<Math.PI/2)) {
		return [(R/(Math.cos(P[0])*Math.cos(P[0])))*V[0],
				(R*Math.sin(P[0])*Math.sin(P[1])/(Math.cos(P[0])*Math.cos(P[0])*Math.cos(P[1])))*V[0]+R/(Math.cos(P[1])*Math.cos(P[1])*Math.cos(P[0]))*V[1]];
	}
	else {
		return [0.00000001,0.00000001];
	}
}

// Gnomonica di centro [0,R,0]
function EqToGnpY(P) {
	if ((P[0]>0) & (P[0]<Math.PI)) {
		return [-R*Math.cos(P[0])/Math.sin(P[0]),R*Math.sin(P[1])/(Math.cos(P[1])*Math.sin(P[0]))];
	}
	else {
		return [1000000000,1000000000];
	}
}

function VecEqToGnpY(P,V) {
	if ((P[0]>0) & (P[0]<Math.PI)) {
		return [(R/(Math.sin(P[0])*Math.sin(P[0])))*V[0],
				-(R*Math.cos(P[0])*Math.sin(P[1])/(Math.sin(P[0])*Math.sin(P[0])*Math.cos(P[1])))*V[0]+R/(Math.cos(P[1])*Math.cos(P[1])*Math.sin(P[0]))*V[1]];
	}
	else {
		return [0.00000001,0.00000001];
	}
}

// Gnomonica di centro [0,0,R]
function EqToGnpZ(P) {
	if (P[1]>Math.PI/18) {
		return [R*Math.cos(P[1])*Math.sin(P[0])/Math.sin(P[1]),-R*Math.cos(P[1])*Math.cos(P[0])/Math.sin(P[1])];
	}
	else {
		return [1000000000,1000000000];
	}
}

function VecEqToGnpZ(P,V) {
	if (P[1]>Math.PI/18) {
		return [(R*Math.cos(P[1])*Math.cos(P[0])/Math.sin(P[1]))*V[0]-R*Math.sin(P[0])/(Math.sin(P[1])*Math.sin(P[1]))*V[1],
				(R*Math.cos(P[1])*Math.sin(P[0])/Math.sin(P[1]))*V[0]+R*Math.cos(P[0])/(Math.sin(P[1])*Math.sin(P[1]))*V[1]];
	}
	else {
		return [0.00000001,0.00000001];
	}
}

function GnToCa(P) {
	return [wgn/2+wgn*P[0]/(2*RaggioGnom),hgn/2-hgn*P[1]/(2*RaggioGnom)];
}

function VecGnToCa(V) {
	return [wgn*V[0]/(2*RaggioGnom),-hgn*V[1]/(2*RaggioGnom)];
}

// Funzioni che portano le coordinate dell'equirettangolare a quelle della fuller cubica
// Specifiche per la Fuller
//					5
//			3	0	1	2
//					4
//	C0=[ 0,-R, 0];
//	C1=[ R, 0, 0];
//	C2=[ 0, R, 0];
//	C3=[-R, 0, 0];
//	C4=[ 0, 0,-R];
//	C5=[ 0, 0, R];

function EqToFu(P) {
	var Imin=0;
	for (var i = 0; i < Centri.length; i++) { 
		if (Ang(EqToS(P),Centri[i])<Ang(EqToS(P),Centri[Imin])) {
			Imin=i;
		}
	}
	ColFu=Imin;
	var PFul=[1000000,1000000];
	var Q = P;
	var QS= EqToS(P);
	
	if (Imin==5) {
		Q = EqToGnpZ(P);
		PFul=[5*wfu/8+wfu*Q[0]/(8*RaggioFu),hfu/6+0*hfu/3-hfu*Q[1]/(6*RaggioFu)];
	}
	if (Imin==4) {
		Q = EqToGnpZ(SToEq([QS[0],QS[1],-QS[2]]));
		PFul=[5*wfu/8+wfu*Q[0]/(8*RaggioFu),hfu/6+2*hfu/3+hfu*Q[1]/(6*RaggioFu)];
	}
	if (Imin==1) {
		Q = EqToGnpX(P);
		PFul=[5*wfu/8+wfu*Q[0]/(8*RaggioFu),hfu/6+1*hfu/3-hfu*Q[1]/(6*RaggioFu)];
	}
	if (Imin==3) {
		Q = EqToGnpX(SToEq([-QS[0],-QS[1],QS[2]]));
		PFul=[1*wfu/8+wfu*Q[0]/(8*RaggioFu),hfu/6+1*hfu/3-hfu*Q[1]/(6*RaggioFu)];
	}
	if (Imin==0) {
		Q = EqToGnpY(SToEq([-QS[0],-QS[1],QS[2]]));
		PFul=[3*wfu/8+wfu*Q[0]/(8*RaggioFu),hfu/6+1*hfu/3-hfu*Q[1]/(6*RaggioFu)];
	}
	if (Imin==2) {
		Q = EqToGnpY(P);
		PFul=[7*wfu/8+wfu*Q[0]/(8*RaggioFu),hfu/6+1*hfu/3-hfu*Q[1]/(6*RaggioFu)];
	}
	return PFul;
}

function VecEqToFu(P,V) {
	var Imin=0;
	for (var i = 0; i < Centri.length; i++) { 
		if (Ang(EqToS(P),Centri[i])<Ang(EqToS(P),Centri[Imin])) {
			Imin=i;
		}
	}
	ColFu=Imin;
	var VFul=[0.00001,0.00001];
	var Q = P;
	var QS= EqToS(P);
	var W = V;
	var WS= VecEqToS(P,V);
	
	if (Imin==1) {
		W = VecEqToGnpX(P,V);
		VFul=[wfu*W[0]/(8*RaggioFu),-hfu*W[1]/(6*RaggioFu)];
	}
	if (Imin==2) {
		W = VecEqToGnpY(P,V);
		VFul=[wfu*W[0]/(8*RaggioFu),-hfu*W[1]/(6*RaggioFu)];
	}
	if (Imin==5) {
		W = VecEqToGnpZ(P,V);
		VFul=[wfu*W[0]/(8*RaggioFu),-hfu*W[1]/(6*RaggioFu)];
	}
	if (Imin==4) {
		QS = SToEq([QS[0],QS[1],-QS[2]]);
		W = VecEqToGnpZ(QS,VecSToEq(QS,[WS[0],WS[1],-WS[2]]));
		VFul=[wfu*W[0]/(8*RaggioFu),hfu*W[1]/(6*RaggioFu)];
	}
	if (Imin==3) {
		QS = SToEq([-QS[0],-QS[1],QS[2]]);
		W = VecEqToGnpX(QS,VecSToEq(QS,[-WS[0],-WS[1],WS[2]]));
		VFul=[wfu*W[0]/(8*RaggioFu),-hfu*W[1]/(6*RaggioFu)];
	}
	if (Imin==0) {
		QS = SToEq([-QS[0],-QS[1],QS[2]]);
		W = VecEqToGnpY(QS,VecSToEq(QS,[-WS[0],-WS[1],WS[2]]));
		VFul=[wfu*W[0]/(8*RaggioFu),-hfu*W[1]/(6*RaggioFu)];
	}
	return VFul;
}

function FuToCa(P) {
	return P;
}
function VecFuToCa(V) {
	return V;
}

function EqToSi(P) {
	return [P[0]*Math.cos(P[1]),P[1]];
}

function SiToCa(P) {
	return [(P[0]+Math.PI)*wsi/(2*Math.PI),((Math.PI)/2-P[1])*(hsi)/(Math.PI)];
}

function VecEqToSi(P,V) {
	return [Math.cos(P[1])*V[0]-P[0]*Math.sin(P[1])*V[1],V[1]];
}

function VecSiToCa(V) {
	return [V[0]*wsi/(2*Math.PI),-V[1]*hsi/(Math.PI)];
}

function EqToGp(P) {
	return [P[0]*R/Math.sqrt(2),R*Math.sqrt(2)*Math.sin(P[1])];
}

function GpToCa(P) {
	return [(P[0]+R*Math.PI/Math.sqrt(2))*wgp/(R*Math.sqrt(2)*Math.PI),(R*Math.sqrt(2)-P[1])*(hgp)/(2*R*Math.sqrt(2))];
}

function VecEqToGp(P,V) {
	return [V[0]*R/Math.sqrt(2),R*Math.sqrt(2)*Math.cos(P[1])*V[1]];
}

function VecGpToCa(V) {
	return [V[0]*wgp/(R*Math.sqrt(2)*Math.PI),-V[1]*hgp/(2*R*Math.sqrt(2))];
}


// Funzione che portano le coordinate dell'equirettangolare a quelle della sfera.
// Si assume che P=(theta,phi) \in [-PI,PI]x[-PI/2,PI/2]
function EqToS(P) {
	return [R*Math.cos(P[1])*Math.cos(P[0]),R*Math.cos(P[1])*Math.sin(P[0]),R*Math.sin(P[1])];
}
// Funzione che portano le coordinate della sfera a quelle dell'equirettangolare.
// Si assume che P=(x,y,z) \in S_R(0,0,0)
function SToEq(P) {
	return [Math.atan2(P[1],P[0]),Math.asin(P[2]/R)];
}

function FEqu(P) {
	return [-R*Math.cos(P[1])*Math.sin(P[0]),R*Math.cos(P[1])*Math.cos(P[0]),0];
}

function FEqv(P) {
	return [-R*Math.sin(P[1])*Math.cos(P[0]),-R*Math.sin(P[1])*Math.sin(P[0]),R*Math.cos(P[1])];
}

function VecEqToS(P,V) {
	return [-R*Math.cos(P[1])*Math.sin(P[0])*V[0]-R*Math.sin(P[1])*Math.cos(P[0])*V[1],
			 R*Math.cos(P[1])*Math.cos(P[0])*V[0]-R*Math.sin(P[1])*Math.sin(P[0])*V[1],
												  R*Math.cos(P[1])*V[1]];
}
// Prende un vettore nello spazio e ne restituisce i coefficienti della proiezione ortogonale sul piano spannato da FEqu(u,v) e FEv(u,v).
// Se il vettore appartiene al piano generato dai due, queste sono le coordinate del vettore sul piano (u,v) rispetto alla base standard. 
// SFRUTTO tantissimo il fatto che il per l'equirettangolare i vettori del frame siano ortogonali in ogni punto: va rifatta per gli altri casi.
function VecSToEq(P,V) {
	return [CoeffProj(FEqu(P),V),CoeffProj(FEqv(P),V)];
}

// restituisce l'unico valore reale tale che la proiezione di V sullo span di B sia a*B.
// Se B è 0 (con buona approssimazione almeno) scula a bestia: restituisce 0.
function CoeffProj(B,V) {
	var temp = ScalarPr(B,B); 
	if (temp < 0.0001) {
		return 0;
	}
	else {
		return ScalarPr(B,V)/temp;
	}
}

// Calcola la distanza tra due punti in R3
function Dist(P,Q) {
	return Math.sqrt((P[0]-Q[0])*(P[0]-Q[0])+(P[1]-Q[1])*(P[1]-Q[1])+(P[2]-Q[2])*(P[2]-Q[2]));
}

// Prende in input un vettore 2D sul piano euclideo e ne restituisce la norma.
function Norm2Eu(V) {
	return Math.sqrt(V[0]*V[0]+V[1]*V[1]);
}
function Norm3Eu(V) {
	return Math.sqrt(V[0]*V[0]+V[1]*V[1]+V[2]*V[2]);
}

function ScalarPr2Eu(V,W) {
	return (V[0]*W[0]+V[1]*W[1]);
}

function Distanza2Eu(V,W) {
	return (V[0]-W[0])*(V[0]-W[0])+(V[1]-W[1])*(V[1]-W[1])
}


// Prende in input un vettore 2D sull'equirettangolare applicato in P e ne restituisce la norma rispetto alla metrica indotta dalla sfera.
function Norm2Eq(P,V) {
	return Math.sqrt(V[0]*V[0]*Math.pow(Math.cos(P[1]),4)+V[1]*V[1]);
}// Calcola il prodotto scalare tra V e W
function ScalarPr(V,W) {
	return (V[0]*W[0]+V[1]*W[1]+V[2]*W[2]);
}

// Calcola il prodotto Wedge tra V e W
function WedgePr(V,W) {
	return [ V[1]*W[2]-V[2]*W[1],
			-V[0]*W[2]+V[2]*W[0],
			 V[0]*W[1]-V[1]*W[0]];
}

// Restituisce l'angolo tra V e W. Forse con segno.
function Ang(V,W) {
	return Math.acos(ScalarPr(V,W)/Math.sqrt(ScalarPr(V,V)*ScalarPr(W,W)));
}

// Normalizza il vettore V dello spazio e poi lo moltiplica per il numero a (puo' essere negativo).
function VNormalize(V,a) {
	var temp = Math.sqrt(ScalarPr(V,V));
	if (temp < 0.0001) {
		return [0,0,0];
	}
	else {
		return [a*V[0]/temp,a*V[1]/temp,a*V[2]/temp];
	}
}

// Proietta un punto sulla sfera di raggio R o restitutisce 0 se il punto è praticamente l'origine.
function ProjS(P) {
	var temp = Math.sqrt(ScalarPr(P,P));
	if (temp < 0.0000001) {
		return [0,0,0];
	}
	else {
		return [R*P[0]/temp,R*P[1]/temp,R*P[2]/temp];
	}
}

// Ruota il punto P\in R^3 attorno all'asse identificato dall'origine e dal vettore V di un angolo theta.
function Ruota(P,V,th) {
	var tVP = ScalarPr(P,V);
	var tVV = ScalarPr(V,V);
	return [
	(V[0]*tVP*(1-Math.cos(th))+tVV*P[0]*Math.cos(th)+Math.sqrt(tVV)*(-V[2]*P[1]+V[1]*P[2])*Math.sin(th))/tVV,
	(V[1]*tVP*(1-Math.cos(th))+tVV*P[1]*Math.cos(th)+Math.sqrt(tVV)*( V[2]*P[0]-V[0]*P[2])*Math.sin(th))/tVV,
	(V[2]*tVP*(1-Math.cos(th))+tVV*P[2]*Math.cos(th)+Math.sqrt(tVV)*(-V[1]*P[0]+V[0]*P[1])*Math.sin(th))/tVV
	];
}