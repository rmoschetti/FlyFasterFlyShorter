// JavaScript Document
/*
if (HighContrast) {
	window.ColorPl1="0,255,0";
	window.ColorPl2="255,255,0";
	
	document.getElementById("CBM").style.visibility = "visible";
	
} else {
	window.ColorPl1="0,255,0";
	window.ColorPl2="255,0,0";
	
	document.getElementById("CBM").style.visibility = "hidden";

}
*/

BloccoTastiera=0;


// Questa funzione si chiama "costructor", serve per creare un nuovo punto date le sue coordinate sul canvas
function Punto(indice,P0, V0, Per,IDtex,NumMod) {
	//Psf e Vsf sono "property", cioè due valori numerici che rappresentano rispettivamente la posizione e il vettore direzione sulla sfera
	this.ID=indice;
	this.IDText=IDtex;
	this.Premuto=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	this.Muovi=1;
	this.Psf = [1,0,0];//EqToS(CaToEq(P0));
	this.Vsf = [1,0,0];//VNormalize(VecEqToS(CaToEq(P0),VecCaToEq(V0)),LungVecSfera);
	this.Nsf = function() {return Ruota(this.Vsf,this.Psf,Math.PI/2)};
	this.Percorso=indice;
	this.Posizione=0;
	this.StampaNumero=0;
	this.Attivo=0;
	this.CambioCartinaEffettuato=0;
	this.NumeroModello=NumMod;
	this.VerticiAereo=new Array(MatricePuntiAereo[this.NumeroModello].length);
	this.TempoImpiegato=0;
	this.ModDisegno=PlaneMode;
	this.CorrezioneRotta=0;
	this.CalibrazioneInCorso=0;
	this.CampioniCalibrazione=0;
	
	//questi sono "methods", cioè delle funzione interne al punto
    this.Pca = function() {return EqToCa(SToEq(this.Psf));};
    this.Vca = function() {return VecEqToCa(VecSToEq(SToEq(this.Psf),this.Vsf));};
    this.Nca = function() {return VecEqToCa(VecSToEq(SToEq(this.Psf),this.Nsf()))};
    this.Peq = function() {return SToEq(this.Psf)};
	this.Veq = function() {return VecSToEq(SToEq(this.Psf),this.Vsf)};
	this.Neq = function() {return VecSToEq(SToEq(this.Psf),this.Nsf())};
	this.Pme = function() {return EqToMe(this.Peq())};
	this.PmeCa = function() {return MeToCa(this.Pme())};
	this.Vme = function() {return VecEqToMe(this.Peq(),this.Veq())};
	this.VmeCa = function() {return VecMeToCa(this.Vme())};
	this.Nme = function() {return VecEqToMe(this.Peq(),this.Neq())};
	this.NmeCa = function() {return VecMeToCa(this.Nme())};
	
	//questi metodi dovrebbero servire a modificare delle proprietà interne
	this.Gira = function(DD) {
		var CorrezioneSensibile=1.4;
		//var Delta=SensTurn*Math.floor((Math.max(-1, Math.min(1, (DD-this.CorrezioneRotta)*CorrezioneSensibile)))*100)/100;
		var Delta=SensTurn*Math.floor((DD-this.CorrezioneRotta)*CorrezioneSensibile*100)/100;
		if (StepProgramma==4) {
			var delta = -Delta*StepTurn;
			this.Vsf=Ruota(this.Vsf,this.Psf,Math.sign(delta)*delta*delta);
			this.GeneraImmagine();
		} else if (StepProgramma==3) {
			if (this.CambioCartinaEffettuato==0 & Delta>0.8 & BloccoTastiera==0) {
				if (timerR==1 || timerL==1 || timerA==1 || timerD==1) BloccoTastiera=1;
				
				this.CambioCartinaEffettuato=1;
				CartineScroll(1);
				//if (TimScorrCartina==0)
				//TimScorrCartina=setInterval(function(){ CartineScroll(1) },500);
			}
			if (this.CambioCartinaEffettuato==0 & Delta<-0.8 & BloccoTastiera==0) {
				if (timerR==1 || timerL==1 || timerA==1 || timerD==1) BloccoTastiera=1;
				this.CambioCartinaEffettuato=1;
				CartineScroll(-1);
				//if (TimScorrCartina==0)
				//TimScorrCartina=setInterval(function(){ CartineScroll(1) },500);
			}
			if (this.CambioCartinaEffettuato==1 & Delta*Delta<0.01) {
				this.CambioCartinaEffettuato=0;
				//clearInterval(TimScorrCartina);
				//TimScorrCartina=0;
			}
		} else if (StepProgramma==1) {
			if (Delta>0.5) {
				AereiInGioco=2;
				document.getElementById("one").style.borderColor="rgba(255,0,0,0)";
				document.getElementById("two").style.borderWidth="7px";
				document.getElementById("two").style.borderColor="rgba(255,0,0,1)";
			}
			if (Delta<-0.5) {
				AereiInGioco=1;
				document.getElementById("two").style.borderColor="rgba(255,0,0,0)";
				document.getElementById("one").style.borderWidth="7px";
				document.getElementById("one").style.borderColor="rgba(255,0,0,1)";
			}	
		} else if (StepProgramma==0) {
			document.getElementById("Range"+this.IDText).value=Math.floor(50*(Delta+1));
			if (Delta>0.7 && document.getElementById("Range"+(1-this.IDText)).value > 70) NuovaFase(0.5);
			
		} else if (StepProgramma==0.5 && this.CalibrazioneInCorso==1) {
			this.CorrezioneRotta=(this.CorrezioneRotta*this.CampioniCalibrazione+DD)/(this.CampioniCalibrazione+1);
			this.CampioniCalibrazione++;
		}
		
		
	}
	
	this.CambiaMod= function() {
		if (this.ModDisegno==1) this.ModDisegno=0;
		else this.ModDisegno++;
	}
	
	this.Anima = function() {
		if (this.Attivo && this.Muovi) {
			var Old=this.Psf;
			this.Psf=Ruota(Old,WedgePr(Old,this.Vsf),VelAng);
			this.Vsf=Ruota(this.Vsf,WedgePr(Old,this.Psf),Ang(Old,this.Psf));
			this.GeneraImmagine();			
			if (this.Posizione<Percorsi[this.Percorso].length) {
				var Diff=[this.Psf[0]-Percorsi[this.Percorso][this.Posizione][0],
						  this.Psf[1]-Percorsi[this.Percorso][this.Posizione][1],
						  this.Psf[2]-Percorsi[this.Percorso][this.Posizione][2]];
				if (ScalarPr(Diff,Diff)<1) {
					if (this.Posizione<Percorsi[this.Percorso].length-1) {
						this.Psf=Percorsi[this.Percorso][this.Posizione];
						this.Posizione++;
						this.Temporizzatore();
						this.GeneraImmagine();
					} else {
						this.Posizione++;
						this.Attivo=0;
						this.TempoImpiegato=new Date() - TempoPartenza;
						ControlloFine();
					}
				}
			}
			
			
		}
	}

this.GeneraImmagine = function() {
		var Norm=this.Nsf();
			//console.log(this.NumeroModello);
			//console.log(this.VerticiAereo.length);
			for (var i=0;i<this.VerticiAereo.length;i++) { //Faccio tutti gli attuali vertici dell'aereo
					var X=MatricePuntiAereo[this.NumeroModello][i][1]; //Mi segno i coefficienti sul piano tangente (potrei toglierlo per ottimizzare)
					var Y=MatricePuntiAereo[this.NumeroModello][i][0];
					//alert(this.Vsf[0]*X+Norm[0]*Y);
					this.VerticiAereo[i]=VNormalize([this.Psf[0]+ScalaCartine[CartinaAttiva]*(this.Vsf[0]*X+Norm[0]*Y),this.Psf[1]+ScalaCartine[CartinaAttiva]*(this.Vsf[1]*X+Norm[1]*Y),this.Psf[2]+ScalaCartine[CartinaAttiva]*(this.Vsf[2]*X+Norm[2]*Y)],R); //Calcolo la posizione dell'I-esimo vertice
			}
	
	}
	

	this.Temporizzatore = function() {
		this.Muovi=0;
		this.StampaNumero=3;
		setTimeout(function() { this.StampaNumero=2; setTimeout(function() { this.StampaNumero=1; setTimeout(function() { this.StampaNumero=0; this.Muovi=1; }.bind(this), 1000);}.bind(this), 1000); }.bind(this), 1000);	
	}
	
	this.Press = function(IdB) {
		if (IdB==2 && this.Premuto[IdB]==0 && StepProgramma==0) {
			Punti[0].Premuto[IdB]=1;
			Punti[1].Premuto[IdB]=1;
			//ScambiaVolanti();
		}
		
		if (StepProgramma==0) {
			//this.Premuto[IdB]=1;
			document.getElementById("TestPulsante"+this.IDText).style.visibility='visible';
		}
		

		if (IdB==3 && this.Premuto[IdB]==0 && StepProgramma==1 && this.Attivo==0) {		
			this.Attivo=1;
			this.Premuto[IdB]=1;
			this.GeneraImmagine();
			NuovaFase(2);
		}
		
		if (IdB==3 && this.Premuto[IdB]==0 && StepProgramma==3) {
				this.Premuto[IdB]=1;
				for (i=0;i<Punti.length;i++) Punti[i].GeneraImmagine();	
				if (ShowInstructions) NuovaFase(3.5);
				else NuovaFase(4);
		}
		
		//Step guida
		
		if (IdB==3 && this.Premuto[IdB]==0 && StepProgramma==3.5) {
				this.Premuto[IdB]=1;
				NuovaFase(3.6);
		}
		
		if (IdB==3 && this.Premuto[IdB]==0 && StepProgramma==3.6) {
				this.Premuto[IdB]=1;
				NuovaFase(3.7);
		}
		
		if (IdB==3 && this.Premuto[IdB]==0 && StepProgramma==3.7) {
				this.Premuto[IdB]=1;
				NuovaFase(3.8);
		}
		
		if (IdB==3 && this.Premuto[IdB]==0 && StepProgramma==3.8) {
				this.Premuto[IdB]=1;
				NuovaFase(4);
		}
		
		//Fine step guida
		
		if (IdB==3 && this.Premuto[IdB]==0 && StepProgramma==5) {
			this.Premuto[IdB]=1;
			NuovaFase(1);
	}
	
		if (IdB==2 && this.Premuto[IdB]==0 && StepProgramma!=0) {
				this.Premuto[IdB]=1;
				 for (var i=0;i<Punti.length;i++) Punti[i].CambiaMod();
		}
		
		if (IdB==1 && this.Premuto[IdB]==0 && StepProgramma!=0) {
			this.Premuto[IdB]=1;
			CambioGriglia();
	}
		
		if (IdB==9 && this.Premuto[IdB]==0 ) {
				this.Premuto[IdB]=1;
				//console.log("reset triggered");
				/*
				if (Punti[1-this.IDText].Attivo==0) {
					for (i=0;i<Punti.length;i++) {
						if (Punti[i].Attivo==1)	
							Punti[i].TempoImpiegato=-1;//new Date() - TempoPartenza;	
					}
					
				}
				*/
				AStop();
			NewTarget();
			for (i=0;i<Punti.length;i++) {
							Punti[i].TempoImpiegato=-1;
							Punti[i].Reset();
					}
				NuovaFase(1);
		
		}
		
		if (IdB==4 && this.Premuto[IdB]==0) {
				this.Premuto[IdB]=1;
				CurrentLang=(CurrentLang % NumeroLingue)+1; 
				console.log("Cambio lingua:" + CurrentLang); 
				Traduci(CurrentLang); 
		}
		
		if (IdB==8 && this.Premuto[IdB]==0 ) {
				this.Premuto[IdB]=1;
				 GoColorBlindMode();
		}
	}
			
	this.Release = function(IdB) {
		this.Premuto[IdB]=0;
		//if (IdB==3 && StepProgramma==0) {
		//	document.getElementById("TestPulsante"+this.IDText).innerHTML=0;		
			
		//}
	}
	
	function Pialla(V) {
		return [Math.floor(V[0]*10000)/10000,Math.floor(V[1]*10000)/10000,Math.floor(V[2]*10000)/10000]	
	}
	
	this.Reset = function() {
		this.Percorso=this.IDText;
		this.Posizione=1;
		this.Psf=Percorsi[this.IDText][0];
		this.Vsf=VNormalize(WedgePr(WedgePr(Percorsi[this.IDText][0],Percorsi[this.IDText][1]),Percorsi[this.IDText][0]),LungVecSfera);
		
		//VNormalize([Percorsi[this.IDText][1][0]-Percorsi[this.IDText][0][0],Percorsi[this.IDText][1][1]-Percorsi[this.IDText][0][1],Percorsi[this.IDText][1][2]-Percorsi[this.IDText][0][2]],LungVecSfera);
		//this.Vsf=[0,0.02,0];[LungVecSfera,0,0];
		this.StampaNumero=0;
		this.Attivo=0;
		this.Muovi=0;	
		this.TempoImpiegato=0;
	}
	
}

function CambioGriglia() {
	if (PresenzaGriglia==0) {
				for (i=0; i<MapNames.length;i++) {
					document.getElementById(NameLayers[i]).style.backgroundImage = "url('" + NameImagesG[i] + "')"; //');";//"url('"+NameImagesG[i]+")';";		
				}
				PresenzaGriglia=1;
			} else {
				for (i=0; i<MapNames.length;i++) {
					document.getElementById(NameLayers[i]).style.backgroundImage = "url('" + NameImages[i] + "')";		
				}
				PresenzaGriglia=0;
			}

}

function Calibra() {
	for (i=0;i<Punti.length;i++) {
			Punti[i].CalibrazioneInCorso=1;
			
			setTimeout(function() {NuovaFase(1)},2000);	
		}
}

function ScambiaVolanti() {
	var A=Punti[0].ID;
	//var B=Punti[0].IDText;
	Punti[0].ID=Punti[1].ID;
	//Punti[0].IDText=Punti[1].IDText;
	Punti[1].ID=A;
	//Punti[1].IDText=B;
}



function CartineScroll(M) {
	for (var i=0;i<6;i++) {
		document.getElementById(NameLayers[i]).style.visibility='hidden';	
	}
	
	if (M==1) {
		CartinaAttiva++;
		if (CartinaAttiva>=6) CartinaAttiva=0;
		
	} else if (M==-1) {
		CartinaAttiva--;
		if (CartinaAttiva<0) CartinaAttiva=5;
		
	}
	
	if (CartinaAttiva==4) {
		document.getElementById("SplashScreenFuller").style.visibility='visible';	
	} else {
		document.getElementById("SplashScreenFuller").style.visibility='hidden';	
	}
	
	document.getElementById("ImgCartina").src=NameImagesP[CartinaAttiva];
	document.getElementById("NomeCartinaAttiva").innerHTML=MapNames[CartinaAttiva][CurrentLang];
	document.getElementById(NameLayers[CartinaAttiva]).style.visibility='visible';
	
}




var AereiInGioco=0;
var Timer;
//var ControlloInizioConteggio=0;
window.StepProgramma=0;


function IniziaPartita() {
	clearInterval(Timer);
	for (i=0;i<Punti.length;i++) if (Punti[i].Attivo) Punti[i].Temporizzatore();
	document.getElementById("SplashScreen1").style.visibility='hidden';
	TempoPartenza=new Date();
	PartenzaAnimazione();		
}


function ControlloFine() {
	var Fine=1;
	for (i=0;i<Punti.length;i++) if (Punti[i].Attivo==1) Fine=0;
	if (Fine==1) {
		NuovaFase(5);
	}
}



// ------------------------------------------------------------------------------------------------
// Funzione init
// ------------------------------------------------------------------------------------------------

GoingCB=0;



function GoColorBlindMode() {
	if (GoingCB==0) {
	//console.log(GoingCB);
		GoingCB=1;
	//console.log(GoingCB);
		for (var i=0;i<Punti.length;i++) {
			Punti[i].NumeroModello=i;
			Punti[i].VerticiAereo=new Array(MatricePuntiAereo[i].length);
			Punti[i].GeneraImmagine();
		}
		window.ColorPl1="255,140,255";
		window.ColorPl2="255,255,0";
	
		document.getElementById("CBM").style.visibility = "visible";
		document.getElementsByName("Tred")[0].setAttribute("name","Tcircle");
		document.getElementsByName("Tgreen")[0].setAttribute("name","Tsquare");
		//document.getElementsByName("Tared")[0].setAttribute("name","Tacircle");
		//document.getElementsByName("Tagreen")[0].setAttribute("name","Tasquare");

	} else {
		GoingCB=0;
		for (var i=0;i<Punti.length;i++) {
			Punti[i].NumeroModello=2;
			Punti[i].VerticiAereo=new Array(MatricePuntiAereo[2].length);
			Punti[i].GeneraImmagine();
		}
		window.ColorPl1="0,255,0";
		window.ColorPl2="255,0,0";
		
		
		document.getElementById("CBM").style.visibility = "hidden";
		document.getElementsByName("Tcircle")[0].setAttribute("name","Tred");
		document.getElementsByName("Tsquare")[0].setAttribute("name","Tgreen");
		//document.getElementsByName("Tacircle").setAttribute("name","Tared");
		//document.getElementsByName("Tasquare").setAttribute("name","Tagreen");
	}
	Traduci(CurrentLang);
	AStop();
	NewTarget();
	
	if (UseGamepad && GoingCB==0) {
		document.getElementById("imaG1").src="files/images/guida1.jpg";
		document.getElementById("imaG2").src="files/images/guida2_gamepad.jpg";
		document.getElementById("ima1p").src="files/images/1p_g.png";
		document.getElementById("ima2p").src="files/images/2p_g.png";
		document.getElementById("DebugScreen").style.visibility='visible';

	}
	if (UseGamepad==0 && GoingCB==0) { 
		document.getElementById("imaG1").src="files/images/guida1.jpg";
		document.getElementById("imaG2").src="files/images/guida2.jpg";
		document.getElementById("ima1p").src="files/images/1p.png";
		document.getElementById("ima2p").src="files/images/2p.png";
	}
	
	if (UseGamepad && GoingCB==1) {
		document.getElementById("imaG1").src="files/images/guida1_CB.jpg";
		document.getElementById("imaG2").src="files/images/guida2_gamepad_CB.jpg";
		document.getElementById("ima1p").src="files/images/1p_g_CB.png";
		document.getElementById("ima2p").src="files/images/2p_g_CB.png";
		document.getElementById("DebugScreen").style.visibility='visible';

	}
	if (UseGamepad==0 && GoingCB==1) { 
		document.getElementById("imaG1").src="files/images/guida1_CB.jpg";
		document.getElementById("imaG2").src="files/images/guida2_CB.jpg";
		document.getElementById("ima1p").src="files/images/1p_CB.png";
		document.getElementById("ima2p").src="files/images/2p_CB.png";
	}
	StepProgramma=0.5;
	NuovaFase(1);
}


function init() {
	//Init specifica del documento, viene chiamata dopo la funzione InitStandard(), nel javascript comune, che già da sola richiama tutti gli eventi click
	window.CartinaAttiva=0;
	
	NumeroLingue=TranslateText[0].length-1;
	
	if (Language==-1) CurrentLang=1;
	else CurrentLang=Language;
	
		
			
	w=document.getElementById("DivCanvas").clientWidth;
	h=document.getElementById("DivCanvas").clientHeight;
	wme=document.getElementById("DivCanvasMe").clientWidth;
	hme=document.getElementById("DivCanvasMe").clientHeight;
	waz=document.getElementById("DivCanvasAzi").clientWidth;
	haz=document.getElementById("DivCanvasAzi").clientHeight;
	wfu=document.getElementById("DivCanvasFul").clientWidth;
	hfu=document.getElementById("DivCanvasFul").clientHeight;
	wsi=document.getElementById("DivCanvasSin").clientWidth;
	hsi=document.getElementById("DivCanvasSin").clientHeight;
	wgp=document.getElementById("DivCanvasGP").clientWidth;
	hgp=document.getElementById("DivCanvasGP").clientHeight;
	
	
	document.getElementById("Canvas").width=w;
	document.getElementById("Canvas").height=h;
	document.getElementById("CanvasMe").width=wme;
	document.getElementById("CanvasMe").height=hme;
	document.getElementById("CanvasAzi").width=waz;
	document.getElementById("CanvasAzi").height=haz;
	document.getElementById("CanvasFul").width=wfu;
	document.getElementById("CanvasFul").height=hfu;
	document.getElementById("CanvasSin").width=wsi;
	document.getElementById("CanvasSin").height=hsi;
	document.getElementById("CanvasGP").width=wgp;
	document.getElementById("CanvasGP").height=hgp;
	
	if (FixedZoom==-1) {
		Ridimensiona();
		window.addEventListener("resize", Ridimensiona);
	} else {
		document.getElementsByTagName("body")[0].style.transform="scale("+FixedZoom+")";
	}
	
	window.ctx=document.getElementById("Canvas").getContext('2d');
	window.ctx2=document.getElementById("CanvasMe").getContext('2d');
	window.ctxazi=document.getElementById("CanvasAzi").getContext('2d');
	window.ctxful=document.getElementById("CanvasFul").getContext('2d');
	window.ctxsin=document.getElementById("CanvasSin").getContext('2d');
	window.ctxgp=document.getElementById("CanvasGP").getContext('2d');
	

	
	
	window.ScalaCartine=[1,1.5,2,1,2,1];
	
	
	window.fps= 25;//Frame al secondo delle animazioni
	window.Interval = 1000/fps;
	window.AnimStart = false;	 //Stato animazioni default
	
	/* Passaggi per la gestione animazioni */
	window.Now;
	window.Then = Date.now();
	
	// Queste mi servono per controllare i click nel canvas
	mousex=0;
	mousey=0;
	mousexo=0;
	mouseyo=0;
		
	TimScorrCartina=0;
	// Raggio della sfera 	
	R=10; 	
	
	OpzioneDisegno=0;
	
	PresenzaGriglia=0;
	// Parametri vari
	VelAng=2*Math.PI/450;
	LungVecSfera = 2*Math.PI*R/450;
	MoltLungVec = 10;
	
	StepTurn = (Math.PI/2)/10;

	Punti=[];
	//Punti=[new Punto([0,h/2],[0,PixelPerFrame]), new Punto([0,h/2],[0,PixelPerFrame])];
	
	//Punti.push(new Punto(5,[0,h/2],[0,PixelPerFrame],1));
	// Specifiche per la mercatore	
	AngUp= 82;
	AngDw=-82;
	MercTruncUp=EqToMe([0,AngUp*Math.PI/180])[1];
	MercTruncDw=EqToMe([0,AngDw*Math.PI/180])[1];
		
	// Specifiche per la gnomonica	
	MinAzimut=30*Math.PI/180;
	RaggioGnom=R*Math.cos(MinAzimut)/Math.sin(MinAzimut);

	// Genera punti
	
	// Specifiche per la Fuller
	//					5
	//			3	0	1	2
	//					4
	C0=[ 0,-R, 0];
	C1=[ R, 0, 0];
	C2=[ 0, R, 0];
	C3=[-R, 0, 0];
	C4=[ 0, 0,-R];
	C5=[ 0, 0, R];

	Centri=[C0,C1,C2,C3,C4,C5];
	MinAzimut=45*Math.PI/180;
	RaggioFu=R*Math.cos(MinAzimut)/Math.sin(MinAzimut);

	ColFu=0;
	Cols=["rgb(255,0,0)","rgb(0,255,0)","rgb(0,0,255)","rgb(255,0,255)","rgb(0,255,255)","rgb(255,255,0)",
		  "rgb(255,0,0)","rgb(0,255,0)","rgb(0,0,255)","rgb(255,0,255)","rgb(0,255,255)","rgb(255,255,0)",
		  "rgb(255,0,0)","rgb(0,255,0)","rgb(0,0,255)","rgb(255,0,255)","rgb(0,255,255)","rgb(255,255,0)",
		  "rgb(255,0,0)","rgb(0,255,0)","rgb(0,0,255)","rgb(255,0,255)","rgb(0,255,255)","rgb(255,255,0)",
		  "rgb(255,0,0)","rgb(0,255,0)","rgb(0,0,255)","rgb(255,0,255)","rgb(0,255,255)","rgb(255,255,0)"];
	
	// Questo controlla se l'animazione è attiva o meno.
	AnimON = false;
	NewTarget();
	Disegna();	

	if (UseGamepad && GoingCB==0) {
		document.getElementById("imaG1").src="files/images/guida1.jpg";
		document.getElementById("imaG2").src="files/images/guida2_gamepad.jpg";
		document.getElementById("ima1p").src="files/images/1p_g.png";
		document.getElementById("ima2p").src="files/images/2p_g.png";
		document.getElementById("DebugScreen").style.visibility='visible';

	}
	if (UseGamepad==0 && GoingCB==0) { 
		document.getElementById("imaG1").src="files/images/guida1.jpg";
		document.getElementById("imaG2").src="files/images/guida2.jpg";
		document.getElementById("ima1p").src="files/images/1p.png";
		document.getElementById("ima2p").src="files/images/2p.png";
	}
	
	if (UseGamepad && GoingCB==1) {
		document.getElementById("imaG1").src="files/images/guida1_CB.jpg";
		document.getElementById("imaG2").src="files/images/guida2_gamepad_CB.jpg";
		document.getElementById("ima1p").src="files/images/1p_g_CB.png";
		document.getElementById("ima2p").src="files/images/2p_g_CB.png";
		document.getElementById("DebugScreen").style.visibility='visible';

	}
	if (UseGamepad==0 && GoingCB==1) { 
		document.getElementById("imaG1").src="files/images/guida1_CB.jpg";
		document.getElementById("imaG2").src="files/images/guida2_CB.jpg";
		document.getElementById("ima1p").src="files/images/1p_CB.png";
		document.getElementById("ima2p").src="files/images/2p_CB.png";
	}
	
	CambioGriglia();
	CambioGriglia();
	
	if (!UseGamepad) InitTastiera();
	
	if (HighContrast) {
		GoingCB=1;
		window.ColorPl1="255,140,255";
		window.ColorPl2="255,255,0";
		
		document.getElementById("CBM").style.visibility = "visible";
		document.getElementsByName("Tcircle")[0].setAttribute("name","Tred");
		document.getElementsByName("Tsquare")[0].setAttribute("name","Tgreen");
	//document.getElementsByName("Tacircle")[0].setAttribute("name","Tared");
	//document.getElementsByName("Tasquare")[0].setAttribute("name","Tagreen");
		
	} else {
		GoingCB=0;
		window.ColorPl1="0,255,0";
		window.ColorPl2="255,0,0";
		
		
		document.getElementById("CBM").style.visibility = "hidden";
	
	}
	
	Traduci(CurrentLang);
	HeadAnimazione();
	
	if (UseGamepad) {
		setInterval(scangamepads, 500);
	}

	document.getElementById("Carica").style.visibility='hidden';

}


function Ridimensiona() {
	ScaleW=window.innerWidth/1920;
	ScaleH=window.innerHeight/1080;
	ScaleFinal=Math.min(ScaleW,ScaleH);
	TranslCentr=Math.floor((window.innerWidth-1920*ScaleFinal)/2);
	document.getElementsByTagName("body")[0].style.transformOrigin="left";
	document.getElementsByTagName("body")[0].style.transform="translate("+TranslCentr+"px) scale("+ScaleFinal+")";
	//document.getElementsByTagName("body")[0].style.transform="";
}


function StampaV(V,n) {
	var temp = "[ " + (Math.round(V[0]*Math.pow(10,n))/Math.pow(10,n));
	for (var i = 1; i < V.length; i++) { 
		temp = temp + ", " + (Math.round(V[i]*Math.pow(10,n))/Math.pow(10,n));
	}
	return temp + "]";
}

function Animazione() {
	for (i=0;i<Punti.length;i++) Punti[i].Anima();	

	//if (new Date() - TempoPartenza>=3000) 
	document.getElementById("NTimer").innerHTML= Math.floor((new Date() - TempoPartenza-3000)/100)/10;
	
	Disegna();
}

function StartStop() {
	/* Questa funzione e' quella da richiamare per far partire, oppure fermare, l'animazione */
	/* TODO: Vedere se si riesce a controllare direttamente da qui l'eventuale pulsante StartStop */
	if (!AnimStart) {
		//HeadAnimazione();
		AnimStart=!AnimStart;
	} else {
		//window.cancelAnimationFrame(IdAnimazione);
		//IdAnimazione=undefined;
		AnimStart=!AnimStart;
	}
}

function AStart() {
	/*Fa partire e se è avviato non fa niente*/	
	if (AnimStart==false) StartStop();
}


function AStop() {
	/*Blocca e se è fermo non fa niente*/
	if (AnimStart==true) StartStop();
}



function HeadAnimazione() {
	/* Questa funzione contiene l'intestazione dell'animazione, in modo da non copiare in tutte le pagine le funzioni relative ai frame al secondo */
	/* TODO: nel caso ci siano pagine senza animazioni, mettere un controllo del tipo 'ifdef animazione then Animazione'*/
	/* Ricordarsi che, nel caso in una pagina ci siano piu di un requestanimationframe, questa cosa potrebbe dare fastidio*/
	
	if (UseGamepad) updateStatus();
	GiraTastiera();
	Now = Date.now();
    Delta = Now - Then;
    if (Delta > Interval) {	
		if (AnimStart) Animazione(); //TODO: qui mettere il controllo ifdef
		Then = Now - (Delta % Interval);
	}
	window.IdAnimazione=window.requestAnimationFrame(HeadAnimazione); //Animazione
}

function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};






/*Compatibilita per funzione requestanimationframe*/

// requestAnimationFrame polyfill by Erik Mšller
// fixes from Paul Irish and Tino Zijdel
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
