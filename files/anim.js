/*
var MatricePuntiMia= [[0,-2.6],[0.77,-2.6],[0.22,-2.08],[0.37,0],[2.64,-1],[2.64,-0.65],[1.5,0.15],[1.5,1],[1.19,1],[1.14,0.58],[0.44,1.14],[0.22,1.4],[0.22,2.8],[0,3.16],[0,0]];
var TriangoliInizialiMia = [[0,1,2],[0,2,14],[2,3,14],[3,4,5],[3,5,6],[3,6,9],[6,8,9],[6,7,8],[3,9,10],[3,10,14],[10,11,14],[11,12,14],[12,13,14]];
*/
var DL=0.2;

function AggiungiPunto(P) {
	for (i=0;i<NuoviPunti.length;i++) {
		if (NuoviPunti[i][0]==P[0] && NuoviPunti[i][1]==P[1]) {
			return i
		} 
	}
	if (i==NuoviPunti.length) {
			NuoviPunti.push(P);
			return NuoviPunti.length-1
	}
}

function AumentaTriangoli() {
	NuoviTriangoli=[];
	NuoviPunti=[];
	for (var i=0; i<TriangoliIniziali.length;i++) {
		var A1=MatricePuntiIniziale[TriangoliIniziali[i][0]];
		var A2=MatricePuntiIniziale[TriangoliIniziali[i][1]];
		var A3=MatricePuntiIniziale[TriangoliIniziali[i][2]];
		
		var B1=AggiungiPunto(A1);
		var B2=AggiungiPunto([(A1[0]+A2[0])/2,(A1[1]+A2[1])/2]);
		var B3=AggiungiPunto(A2);
		var B4=AggiungiPunto([(A1[0]+A3[0])/2,(A1[1]+A3[1])/2]);
		var B5=AggiungiPunto([(A2[0]+A3[0])/2,(A2[1]+A3[1])/2]);
		var B6=AggiungiPunto(A3);

		NuoviTriangoli.push([B1,B2,B4]);
		NuoviTriangoli.push([B2,B3,B5]);
		NuoviTriangoli.push([B2,B4,B5]);
		NuoviTriangoli.push([B4,B5,B6]);

	}
	
}


	var MatricePuntiAereo=new Array(3);
	var	TriangoliAereo=new Array(3);


	var MatricePuntiIniziale = [[-3,0],[3,0],[0,4],[1,0-2],[0.707106,0.707106-2],[0,1-2],[-0.707106,0.707106-2],[-1,0-2],[-0.707106,-0.707106-2],[0,-1-2],[0.707106,-0.707106-2]];
	
	var TriangoliIniziali = [[0,1,2],[3,4,5],[3,5,6],[3,6,7],[3,7,8],[3,8,9],[3,9,10]];
	
	
	var Molt=1.7;
	for (i=0; i<MatricePuntiIniziale.length;i++) {
		MatricePuntiIniziale[i]=[MatricePuntiIniziale[i][0]*Molt,MatricePuntiIniziale[i][1]*Molt];
	}
	
	
	var NuoviPunti=[];
	var NuoviTriangoli=[];



	AumentaTriangoli();
	MatricePuntiIniziale=NuoviPunti;
	TriangoliIniziali=NuoviTriangoli;
	
	AumentaTriangoli();
	
	if (!LowRes) {
		MatricePuntiIniziale=NuoviPunti;
		TriangoliIniziali=NuoviTriangoli;
		AumentaTriangoli();
	}
	
	MatricePuntiAereo[0]=NuoviPunti.slice();
	TriangoliAereo[0]=NuoviTriangoli.slice();
	//console.log(MatricePuntiAereo);
	
	
	var MatricePuntiIniziale = [[-3,0],[3,0],[0,4],[-1,-1],[1,-1],[-1,-3],[1,-3]];
	var TriangoliIniziali = [[0,1,2],[3,4,5],[4,5,6]];
	
	
	var Molt=1.7;
	for (i=0; i<MatricePuntiIniziale.length;i++) {
		MatricePuntiIniziale[i]=[MatricePuntiIniziale[i][0]*Molt,MatricePuntiIniziale[i][1]*Molt];
	}
	
	
	var NuoviPunti=[];
	var NuoviTriangoli=[];


	//console.log(MatricePuntiAereo);
	AumentaTriangoli();
	MatricePuntiIniziale=NuoviPunti;
	TriangoliIniziali=NuoviTriangoli;
	
	AumentaTriangoli();
	
	if (!LowRes) {
		MatricePuntiIniziale=NuoviPunti;
		TriangoliIniziali=NuoviTriangoli;
		AumentaTriangoli();
	}
	
	MatricePuntiAereo[1]=NuoviPunti.slice();;
	TriangoliAereo[1]=NuoviTriangoli.slice();;
	


	var MatricePuntiIniziale=[[0,-2.6],[0.77,-2.6],[0.22,-2.08],[0.37,0],[2.64,-1],[2.64,-0.65],[1.5,0.15],[1.5,1],[1.19,1],[1.14,0.58],[0.44,1.14],[0.22,1.4],[0.22,2.8],[0,3.16],[0,0],[-0.77,-2.6],[-0.22,-2.08],[-0.37,0],[-2.64,-1],[-2.64,-0.65],[-1.5,0.15],[-1.5,1],[-1.19,1],[-1.14,0.58],[-0.44,1.14],[-0.22,1.4],[-0.22,2.8]]; //[-3,-3],[3,-3],[0,3],[-3-DL,-3-DL],[3+DL,-3-DL],[0,3+DL]
	var TriangoliIniziali=[[0,1,2],[0,2,14],[2,3,14],[3,4,5],[3,5,6],[3,6,9],[6,8,9],[6,7,8],[3,9,10],[3,10,14],[10,11,14],[11,12,14],[12,13,14],[0,15,16],[0,16,14],[16,17,14],[17,18,19],[17,19,20],[17,20,23],[20,22,23],[20,21,22],[17,23,24],[17,24,14],[24,25,14],[25,26,14],[26,13,14]]; //[27,30,31],[27,28,31],[31,28,32],[28,29,32],[32,29,27],[32,27,30]
	
	var Molt=1.5;
	for (i=0; i<MatricePuntiIniziale.length;i++) {
		
		MatricePuntiIniziale[i]=[MatricePuntiIniziale[i][0]*Molt,MatricePuntiIniziale[i][1]*Molt];
	}
	
	var NuoviPunti=[];
	var NuoviTriangoli=[];


	AumentaTriangoli();
	
	if (!LowRes) {
		MatricePuntiIniziale=NuoviPunti;
		TriangoliIniziali=NuoviTriangoli;
		AumentaTriangoli();
	}
	
	MatricePuntiAereo[2]=NuoviPunti;
	TriangoliAereo[2]=NuoviTriangoli;
	


// ------------------------------------------------------------------------------------------------
// Funzione che disegna tutto
// ------------------------------------------------------------------------------------------------




function DisegnaTriangolo(cx,a,b,c,Col) {
	cx.beginPath();
	if (Col==1) {
		cx.fillStyle="rgb("+ColorPl1+")";
		cx.strokeStyle="rgb("+ColorPl1+")";	
	} else {
		cx.fillStyle="rgb("+ColorPl2+")";
		cx.strokeStyle="rgb("+ColorPl2+")";
	}
  	cx.moveTo(a[0], a[1]);
  	cx.lineTo(b[0], b[1]);
  	cx.lineTo(c[0], c[1]);
  	cx.closePath();
  	cx.fill();
	cx.stroke();
}


function Disegna() {	
	if (CartinaAttiva==0) ctx.clearRect(-w,-h,3*w,3*h);
	if (CartinaAttiva==1) ctx2.clearRect(-wme,-hme,3*wme,3*hme);	
	if (CartinaAttiva==2) ctxazi.clearRect(-waz,-haz,3*waz,3*haz);	
	if (CartinaAttiva==4) ctxful.clearRect(-wfu,-hfu,3*wfu,3*hfu);	
	if (CartinaAttiva==5) ctxsin.clearRect(-wsi,-hsi,3*wsi,3*hsi);	
	if (CartinaAttiva==3) ctxgp.clearRect(-wgp,-hgp,3*wgp,3*hgp);	
		
	for (i=0;i<Punti.length;i++) {
		if (Punti[i].Attivo) {
			NonUltimo=Punti[i].Posizione < Percorsi[Punti[i].Percorso].length-1;
			if (CartinaAttiva==0) {
				// Equirettangolare
				P0=Punti[i].Pca();
				V0=Punti[i].Vca();
				N0=Punti[i].Nca();
				B0=EqToCa(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione]));
				Context=ctx;
				if (NonUltimo) B1=EqToCa(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione+1]));
				
				
				
				if (Punti[i].ModDisegno==0) {
					for (j=0;j<TriangoliAereo[Punti[i].NumeroModello].length;j++) {
					//if (typeof Punti[i].VerticiAereo[TriangoliAereo[j][0]] == 'undefined') alert(Punti[i].VerticiAereo[TriangoliAereo[j][1]]);
						//alert(Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						//alert(i + " --- " + TriangoliAereo[j][0] + " ---- " + Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						var A=EqToCa(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][0]]));
						var B=EqToCa(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][1]]));
						var C=EqToCa(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][2]]));
						var Soglia=15000;
						if (Distanza2Eu(A,B)<Soglia && Distanza2Eu(A,C)<Soglia) DisegnaTriangolo(Context,A,B,C,i);
					}
				}
				
			} else if (CartinaAttiva==1) {
				// Mercatore
				P0=Punti[i].PmeCa();
				V0=Punti[i].VmeCa();
				N0=Punti[i].NmeCa();
				B0=MeToCa(EqToMe(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione])));	
				if (NonUltimo) B1=MeToCa(EqToMe(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione+1])));
				Context=ctx2;
				
				if (Punti[i].ModDisegno==0) {
					for (j=0;j<TriangoliAereo[Punti[i].NumeroModello].length;j++) {
					//if (typeof Punti[i].VerticiAereo[TriangoliAereo[j][0]] == 'undefined') alert(Punti[i].VerticiAereo[TriangoliAereo[j][1]]);
						//alert(Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						//alert(i + " --- " + TriangoliAereo[j][0] + " ---- " + Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						var A=MeToCa(EqToMe(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][0]])));
						var B=MeToCa(EqToMe(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][1]])));
						var C=MeToCa(EqToMe(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][2]])));
						var Soglia=15000;
						if (Distanza2Eu(A,B)<Soglia && Distanza2Eu(A,C)<Soglia) DisegnaTriangolo(Context,A,B,C,i);
					}
				}
	
			} else if (CartinaAttiva==2) {
				
				//Azimutale equidistante		
				ctxazi.fillStyle=Cols[i];
				P0=AzToCa(EqToAz(Punti[i].Peq()));
				V0=VecAzToCa(VecEqToAz(Punti[i].Peq(),Punti[i].Veq()));
				N0=VecAzToCa(VecEqToAz(Punti[i].Peq(),Punti[i].Neq()));
				B0=AzToCa(EqToAz(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione])));	
				if (NonUltimo) B1=AzToCa(EqToAz(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione+1])));	
				Context=ctxazi;
				
				if (Punti[i].ModDisegno==0) {
					for (j=0;j<TriangoliAereo[Punti[i].NumeroModello].length;j++) {
					//if (typeof Punti[i].VerticiAereo[TriangoliAereo[j][0]] == 'undefined') alert(Punti[i].VerticiAereo[TriangoliAereo[j][1]]);
						//alert(Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						//alert(i + " --- " + TriangoliAereo[j][0] + " ---- " + Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						var A=AzToCa(EqToAz(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][0]])));
						var B=AzToCa(EqToAz(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][1]])));
						var C=AzToCa(EqToAz(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][2]])));
						var Soglia=15000;
						if (Distanza2Eu(A,B)<Soglia && Distanza2Eu(A,C)<Soglia) DisegnaTriangolo(Context,A,B,C,i);
					}
				}
				
			} else if(CartinaAttiva==4) {
		
				//Fuller		
				P0=FuToCa(EqToFu(Punti[i].Peq()));
				V0=VecFuToCa(VecEqToFu(Punti[i].Peq(),Punti[i].Veq()));
				N0=VecFuToCa(VecEqToFu(Punti[i].Peq(),Punti[i].Neq()));
				B0=FuToCa(EqToFu(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione])));		
				if (NonUltimo) B1=FuToCa(EqToFu(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione+1])));	
				Context=ctxful;
				
				if (Punti[i].ModDisegno==0) {
					for (j=0;j<TriangoliAereo[Punti[i].NumeroModello].length;j++) {
					//if (typeof Punti[i].VerticiAereo[TriangoliAereo[j][0]] == 'undefined') alert(Punti[i].VerticiAereo[TriangoliAereo[j][1]]);
						//alert(Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						//alert(i + " --- " + TriangoliAereo[j][0] + " ---- " + Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						var A=FuToCa(EqToFu(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][0]])));
						var B=FuToCa(EqToFu(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][1]])));
						var C=FuToCa(EqToFu(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][2]])));
						var Soglia=15000;
						if (Distanza2Eu(A,B)<Soglia && Distanza2Eu(A,C)<Soglia) DisegnaTriangolo(Context,A,B,C,i);
					}
				}
				
			} else if (CartinaAttiva==5) {
		
				//Sinusoidale		
				P0=SiToCa(EqToSi(Punti[i].Peq()));
				V0=VecSiToCa(VecEqToSi(Punti[i].Peq(),Punti[i].Veq()));
				N0=VecSiToCa(VecEqToSi(Punti[i].Peq(),Punti[i].Neq()));
				B0=SiToCa(EqToSi(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione])));	
				if (NonUltimo) B1=SiToCa(EqToSi(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione+1])));
				Context=ctxsin;
				
				if (Punti[i].ModDisegno==0) {
					for (j=0;j<TriangoliAereo[Punti[i].NumeroModello].length;j++) {
					//if (typeof Punti[i].VerticiAereo[TriangoliAereo[j][0]] == 'undefined') alert(Punti[i].VerticiAereo[TriangoliAereo[j][1]]);
						//alert(Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						//alert(i + " --- " + TriangoliAereo[j][0] + " ---- " + Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						var A=SiToCa(EqToSi(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][0]])));
						var B=SiToCa(EqToSi(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][1]])));
						var C=SiToCa(EqToSi(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][2]])));
						var Soglia=15000;
						if (Distanza2Eu(A,B)<Soglia && Distanza2Eu(A,C)<Soglia) DisegnaTriangolo(Context,A,B,C,i);
					}
				}
				
			} else if (CartinaAttiva==3) {
				
				//Gall-Peters		
				P0=GpToCa(EqToGp(Punti[i].Peq()));
				V0=VecGpToCa(VecEqToGp(Punti[i].Peq(),Punti[i].Veq()));
				N0=VecGpToCa(VecEqToGp(Punti[i].Peq(),Punti[i].Neq()));
				B0=GpToCa(EqToGp(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione])));	
				if (NonUltimo) B1=GpToCa(EqToGp(SToEq(Percorsi[Punti[i].Percorso][Punti[i].Posizione+1])));
				Context=ctxgp;
				
				if (Punti[i].ModDisegno==0) {
					for (j=0;j<TriangoliAereo[Punti[i].NumeroModello].length;j++) {
					//if (typeof Punti[i].VerticiAereo[TriangoliAereo[j][0]] == 'undefined') alert(Punti[i].VerticiAereo[TriangoliAereo[j][1]]);
						//alert(Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						//alert(i + " --- " + TriangoliAereo[j][0] + " ---- " + Punti[i].VerticiAereo[TriangoliAereo[j][0]]);
						var A=GpToCa(EqToGp(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][0]])));
						var B=GpToCa(EqToGp(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][1]])));
						var C=GpToCa(EqToGp(SToEq(Punti[i].VerticiAereo[TriangoliAereo[Punti[i].NumeroModello][j][2]])));
						var Soglia=15000;
						if (Distanza2Eu(A,B)<Soglia && Distanza2Eu(A,C)<Soglia) DisegnaTriangolo(Context,A,B,C,i);
					}
				}
				
			}
		

			if (i==0) Context.fillStyle = "rgba("+ColorPl2+", 1)";
			else  Context.fillStyle = "rgba("+ColorPl1+", 1)";
			Context.beginPath();
			if (i==0) Context.arc(B0[0],B0[1],15,0,2*Math.PI,true);
			else Context.fillRect(B0[0]-13,B0[1]-13,26,26);
			Context.closePath();
			Context.fill();
			Context.fillStyle =  "rgb(0, 0, 0)";
			Context.textAlign="center"; 
			Context.textBaseline = 'middle';
			Context.font="22px Verdana";
			Context.fillText(Punti[i].Posizione,B0[0],B0[1]-1);
				
	
			if (NonUltimo)  {			
				if (i==0) Context.fillStyle = "rgba("+ColorPl2+", 0.7)";
				else  Context.fillStyle = "rgba("+ColorPl1+", 0.7)";
				Context.beginPath();
				if (i==0) Context.arc(B1[0],B1[1],10,0,2*Math.PI,true);
				else Context.fillRect(B1[0]-7,B1[1]-7,14,14);
				Context.closePath();
				Context.fill();
			}
	
		
			if (Punti[i].ModDisegno==1) {
				Context.lineWidth = 4;
				Context.strokeStyle="rgb(255,0,255)";
				Context.beginPath();
				Context.moveTo(P0[0],P0[1]);
				Context.lineTo(P0[0]+MoltLungVec*V0[0],P0[1]+MoltLungVec*V0[1]);
				Context.stroke();
				Context.strokeStyle="rgb(0,255,255)";
				Context.beginPath();
				Context.moveTo(P0[0],P0[1]);
				Context.lineTo(P0[0]+MoltLungVec*N0[0],P0[1]+MoltLungVec*N0[1]);
				Context.stroke();
				Context.lineWidth = 1;
				if (i==0) Context.fillStyle = "rgba("+ColorPl2+", 1)";
				else  Context.fillStyle = "rgba("+ColorPl1+", 1)";
				Context.beginPath();
				if (i==0) Context.arc(P0[0],P0[1],13,0,2*Math.PI,true);
				else Context.fillRect(P0[0]-10,P0[1]-10,20,20);
				
				Context.closePath();
				Context.fill();
				
			} 
		
			if (Punti[i].StampaNumero != 0) {
			Context.fillStyle =  "rgb(255, 255, 255)";
				Context.beginPath();
				if (i==0) Context.arc(P0[0],P0[1],13,0,2*Math.PI,true);
				else Context.fillRect(P0[0]-10,P0[1]-10,20,20);
				Context.closePath();
				
				if (i==0) Context.strokeStyle =  "rgb("+ColorPl2+")";
				else Context.strokeStyle =  "rgb("+ColorPl1+")";
				Context.fill();	
				Context.stroke();
				Context.fillStyle =  "rgb(0, 0, 0)";
				Context.textAlign="center"; 
				Context.textBaseline = 'middle';
				Context.font="20px Verdana";
				Context.fillText(Punti[i].StampaNumero,P0[0],P0[1]);	
			}
		}
	
	}
}

	Percorsi=new Array(2);

function NewTarget() {
	
	LunghezzaPercorso=LengthPath+1;
	Percorsi[0]=new Array(LunghezzaPercorso);
	Percorsi[1]=new Array(LunghezzaPercorso);
	AngoloTotale=0;
	/*
	NN=Math.sqrt(R*R + 5*5);
	Percorsi[0][0]=[0,R*R/NN,R*5/NN];
	Percorsi[1][0]=[0,-R*R/NN,-R*5/NN];
	
	NN=Math.sqrt(R*R);
	Percorsi[0][1]=[0,R*R/NN,0];
	Percorsi[1][1]=[0,-R*R/NN,0];
	*/
	/*NN=Math.sqrt(R*R+1*1);
	Percorsi[0][0]=[R*1,0,0];
	Percorsi[1][0]=[R*1,0,0];//[0,-R*R/NN,-R*1/NN];
	
	NN=Math.sqrt(R*R);
	MM=Math.sqrt(2*2+5*5);
	Percorsi[0][1]=[0,R*R/NN,0];
	Percorsi[1][1]=[R*2/MM,R*0/MM,R*5*5/MM];
	*/
		for (j=0;j<LunghezzaPercorso;j++) {
			X=(Math.random()-0.5);
			Y=(Math.random()-0.5);
			Z=(Math.random()-0.5);
			NN=Math.sqrt(X*X + Y*Y + Z*Z);
			var ControlloAggiunta=1;
			for (var k=0;k<j;k++) {
				if (Dist([R*X/NN,R*Y/NN,R*Z/NN],Percorsi[0][k]) < 1.5 || Dist([R*X/NN,R*Y/NN,R*Z/NN],Percorsi[1][k]) < 1.5 || SToEq([R*X/NN,R*Y/NN,R*Z/NN])[1] > Math.PI/2 - 0.2 || SToEq([R*X/NN,R*Y/NN,R*Z/NN])[1] < -Math.PI/2 + 0.2)  {
					ControlloAggiunta=0;
					j--;
					break;	
				}	
			}
			
			if (ControlloAggiunta==1) {			
				Percorsi[0][j]=[R*X/NN,R*Y/NN,R*Z/NN];
				Percorsi[1][j]=[-R*X/NN,-R*Y/NN,-R*Z/NN];
				//if (j>0) AngoloTotale+=Ang(Percorsi[0][j-1],Percorsi[0][j]); 
			}
		}
		
		//TempoOttimale=(AngoloTotale-6/R)/(25*2*Math.PI/450)+3*LunghezzaPercorso-6;
		//document.getElementById("TempoOttimaleS").innerHTML=Math.floor(TempoOttimale*10)/10 + " secondi";
}

// ------------------------------------------------------------------------------------------------
// Funzioni che regolano il dialogo tra gli oggetti HTML (slider e affini) e il canvas
// ------------------------------------------------------------------------------------------------

function PartenzaAnimazione() {
	window.fps=25;
	window.Interval = 1000/fps;
	StartStop();
	document.getElementById("DivButtStart").innerHTML=(AnimON ? "Start" : "Stop");
	AnimON = ! AnimON;	
}