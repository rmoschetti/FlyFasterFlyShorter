// ----------------------------------------------------- //
//                  GAME CONFIGURATION                   //
// ----------------------------------------------------- //	

// Show instructions before the beginning of the game
ShowInstructions=1;

// Set a fixed zoom (-1 is disabled, a positive number is the Zoom to set)
FixedZoom=-1;

// Default mode (0 = planes, 1 = vectors)
PlaneMode=0;

// Current Language (1=English, 2=Italian, 3=French, 4=Esperanto)
Language=1;

// High contrast mode
HighContrast=0;

//The total number of checkpoints
LengthPath=6;

//Low resolution for the planes
LowRes=0;

//Sensibility of the turning
SensTurn=1.1;

// ----------------------------------------------------- //
//                  GAMEPAD CONFIGURATION                //
// ----------------------------------------------------- //	

// Enable the use of gamepads
UseGamepad=0;

// ID of the axis to turn the aircraft
Id_axis=2;

// Gamepad Buttons configuration [Toggle Grid, Plane Mode, Ok, ResetGame, Change Language]
GamepadButtonList=[1,2,3,9,0,8];

// Gamepad vendor (to avoid multiple gamepad connected, -1 is disabled)
GamepadVendor="0079";

