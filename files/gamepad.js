
	
	
// ----------------------------------------------------- //	
NumeroController=0;
if (UseGamepad) {	
	var haveEvents = 'GamepadEvent' in window;
	var controllers = {};
	var rAF = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
		window.webkitRequestAnimationFrame;
	var cAF = window.cancelAnimationFrame;
}
	
function addgamepad(gamepad) {
	controllers[gamepad.index] = gamepad; 
	if (HighContrast) Punti.push(new Punto(gamepad.index,[w/2,h/2],[1,0],1,NumeroController,NumeroController));
	else Punti.push(new Punto(gamepad.index,[w/2,h/2],[1,0],1,NumeroController,2));
	//rAF(updateStatus);
	NumeroController++;
}
	

/*
	function disconnecthandler(e) {
		removegamepad(e.gamepad);
	}
	*/

function removegamepad(gamepad) {
	var d = document.getElementById("controller" + gamepad.index);
	document.body.removeChild(d);
	delete controllers[gamepad.index];
}
	
function updateStatus() {
	scangamepads();
	
	var ElencoTastiInterni=[1,2,3,9,4,8];
	
	for (j in controllers) {
		var controller = controllers[j];
		
		for (k=0; k<GamepadButtonList.length; k++) {
			var K=GamepadButtonList[k];
			var val = controller.buttons[K];
			if (typeof(val) == "object") {
				pressed = val.pressed;
			}
			if (pressed) {
				for (i=0;i<Punti.length;i++) {
					if (Punti[i].ID==j) Punti[i].Press(ElencoTastiInterni[k]);
				}
			} else {
				for (i=0;i<Punti.length;i++) {
					if (Punti[i].ID==j) Punti[i].Release(ElencoTastiInterni[k]);
				}
			}
		}
		

		PosizioneVolante = controller.axes[Id_axis];
		
		for (i=0;i<Punti.length;i++) {
			if (Punti[i].ID==j) Punti[i].Gira(PosizioneVolante);
		}	
	}
	//rAF(updateStatus);
}

	function scangamepads() {
		//alert();
		gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
		for (var i = 0; i < gamepads.length; i++) {
			if (gamepads[i]) {
				
				if (GamepadVendor==-1 || gamepads[i].id.search(GamepadVendor) != -1) {
					if (!(gamepads[i].index in controllers)) {
						//alert("Aggiungo " + gamepads[i].id);
						//alert(gamepads[i].id.search("0079"));
						addgamepad(gamepads[i]);
					} else {
						controllers[gamepads[i].index] = gamepads[i];
					}
				}
			}
		}
	}