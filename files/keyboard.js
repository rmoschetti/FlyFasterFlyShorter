timerL=0;
timerR=0;
timerA=0;
timerD=0;
TimerTastiera=15;

function GiraTastiera() {
	if (timerL) Punti[0].Gira(-1);
	if (timerR) Punti[0].Gira(1);
	if (timerA) Punti[1].Gira(-1);
	if (timerD) Punti[1].Gira(1);
}


function AbilitaTasti() {
	//controllo tastiera
	document.addEventListener('keydown', function(event) {
	if(event.keyCode == 32) {
       if (StepProgramma==0) {
	   		AggiuntaAerei();
	   } else {
		   
	   }
    }	
	
	if (event.keyCode == 76) { // l  -  Cambia il linguaggio 
    	CurrentLang=(CurrentLang % NumeroLingue)+1; 
    	Traduci(CurrentLang); 
	} 
		
    if(event.keyCode == 37) {
       //if (timerL==0) timerL = setInterval(function() {Punti[0].Gira(-1,1.2);}, TimerTastiera);
       timerL=1;
    }
    else if(event.keyCode == 39) {
       //if (timerR==0) timerR = setInterval(function() {Punti[0].Gira(1,1.2);}, TimerTastiera);
       timerR=1
    }
	
	if(event.keyCode == 38) {
       Punti[0].Press(3);
    }
	
	if(event.keyCode == 65) {
       //if (timerA==0) timerA = setInterval(function() {Punti[1].Gira(-1,1.2);}, TimerTastiera);
       timerA=1;
    }
    else if(event.keyCode == 68) {
       //if (timerD==0) timerD = setInterval(function() {Punti[1].Gira(1,1.2);}, TimerTastiera);
       timerD=1;
    }
	if(event.keyCode == 87) {
       Punti[1].Press(3);
    }
	
	if(event.keyCode == 86) {
       for (var i=0;i<Punti.length;i++) Punti[i].CambiaMod();
    }
	
	if(event.keyCode == 71) {
       CambioGriglia();
    }
	
	if(event.keyCode == 80) {
       GoColorBlindMode();
    }
	
	if(event.keyCode == 79) {
       //Punti[0].Press(9);
       AStop();
	   NewTarget();
       NuovaFase(1);
       
    }
	
	});
	
	document.addEventListener('keyup', function(event) {
    if(event.keyCode == 37) {
       //clearInterval(timerL);
       timerL=0;
       Punti[0].Gira(0);
       BloccoTastiera=0;
    }
    else if(event.keyCode == 39) {
       //clearInterval(timerR);
       timerR=0;
       Punti[0].Gira(0);
    BloccoTastiera=0;
    }
	if(event.keyCode == 38) {
       Punti[0].Release(3);
    }
	
	if(event.keyCode == 65) {
       //clearInterval(timerA);
       timerA=0;
       Punti[1].Gira(0);
       BloccoTastiera=0;
    }
    else if(event.keyCode == 68) {
       //clearInterval(timerD);
       timerD=0;
       Punti[1].Gira(0);
       BloccoTastiera=0;
    }
	if(event.keyCode == 87) {
       Punti[1].Release(3);
    }
	
	});

}



function InitTastiera() {
	
	AggiuntaAerei();
	AbilitaTasti();
	document.getElementById("DebugScreen").style.visibility='hidden';
	//AbilitaTasti();
	StepProgramma=0.5;
	NuovaFase(1);
	
}


function AggiuntaAerei() {
//La chiamo quando premo la barra spaziatrice dalla tastiera: se ci sono due gamepad.. bene! Se invece non ci sono ne aggiunge il numero giusto senza vendor

if (Punti.length<2) {
	for (var ni=Punti.length;ni<2;ni++) {
		if (HighContrast) Punti.push(new Punto(-1,[w/2,h/2],[1,0],1,NumeroController,ni));
		else Punti.push(new Punto(-1,[w/2,h/2],[1,0],1,NumeroController,2));
		NumeroController++;
	}
}
else if (Punti.length>2) {
	//deve essere successo qualche pasticcio con l'aggiunta dei gamepad. Che si fa?
	console.warn("Vedo piu di due gamepad!");
}
	
}